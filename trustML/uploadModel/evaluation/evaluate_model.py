from sklearn.externals import joblib
from sklearn.metrics import confusion_matrix
import numpy as np
def get_model_and_data(model_file, data_file):
    model = joblib.load(model_file)
    # data = np.loadtxt(data_file,delimiter=",",dtype=float)
    # print(data[0])
    data = np.loadtxt(data_file,delimiter=",", dtype=float)
    return model, data

# get a confusion matrix which will be used later
def get_confusion_matrix(model,data):
    import logging
    logging.basicConfig(filename='mylog.log', level=logging.DEBUG)
    """
    :param model:
    :param data:
    :return: a confusion matrix with Y-axis as true label X-axis as predict label
    """
    my_model = model
    # logging.debug("result: ")
    # logging.debug(data[:, :data.shape[1] - 1])
    result = my_model.predict(data[:,:data.shape[1]-1])
    cm = confusion_matrix(data[:,data.shape[1]-1],result)
    return cm

def get_precision_recall_accuracy(confusion_matrix):
    """
    :param confusion_matrix: get from sklearn package
    :return: precisions, recalls -> arrays, accuracy -> a float number
    """
    if confusion_matrix.shape[0] == 2:
        recall = np.zeros(1)
        precision = np.zeros(1)
        precision[0] = float(confusion_matrix[0,0])/(confusion_matrix[0,0]+confusion_matrix[1,0])
        recall[0] = float(confusion_matrix[0,0])/(confusion_matrix[0,0] + confusion_matrix[0,1])
        accuracy = float(confusion_matrix[0,0]+confusion_matrix[1,1])/np.sum(confusion_matrix)
    else:
        recall = np.zeros(confusion_matrix.shape[0])
        precision = np.zeros(confusion_matrix.shape[0])
        correctly_predicted = 0
        for row_idx in range(confusion_matrix.shape[0]):
            precision[row_idx] = float(confusion_matrix[row_idx][row_idx])/ np.sum(confusion_matrix[:,row_idx])
            recall[row_idx] = float(confusion_matrix[row_idx][row_idx])/np.sum(confusion_matrix[row_idx,:])
            correctly_predicted += confusion_matrix[row_idx][row_idx]
        accuracy =  float(correctly_predicted)/float(sum(confusion_matrix))
    return precision, recall, accuracy


