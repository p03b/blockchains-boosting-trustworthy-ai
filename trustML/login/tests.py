from django.test import TestCase
from django.utils import timezone
from .models import MyUserManager, User
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin
from django.urls import reverse
# Create your tests here.
def create_user(t_email, t_password):

    return User.objects.create(email=t_email, password=t_password)
class test_userinformation(TestCase):
    def test_staff(self):
        newuser = create_user('jam233@jam233.com','11aaBB!!')
        #newuser = User(email='jam233@jam233.com')
        self.assertIs(newuser.is_staff, False)
    def test_active(self):
        newuser = User(email='jam233@jam233.com')
        #print(newuser.email)
        self.assertIs(newuser.is_active, True)
    def test_create(self):
        newuser = User(email='jam233.com')
    # #    print(newuser.email)
        self.assertIs(newuser.is_active, True)
    def test_staffa(self):
        newuser = User(email='jam233.com',password='11aaBB!!')
        self.assertIs(newuser.is_staff, False)
