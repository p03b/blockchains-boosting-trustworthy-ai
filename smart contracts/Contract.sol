/*
The contract should:
    1. Initialise the contract to work with a specific, given model.
    2. Let anybody send datasets to the contract.
    3. Compare the datasets to the training dataset of the model and get their similarity.
    4. At the end of the contract period, pay out to the top k- datasets ranked by similarity.
*/

pragma solidity ^0.4.24;

contract P03Contract {
    //List of people who provide datasets for testing on the model.
    address[] public users;
    //A mappng from the users address to the dataset they provided.
    mapping(address => address) public datasetsMap;
    //A mapping from the users address to the similarity of the dataset to the training dataset.
    mapping(address => uint256) public similarityMap;
    //A mapping from the users address to the evaluation of their dataset on the model.
    mapping(address => uint256) public evaluationMap;
    //List of the k top users who provided the best datasets, to pay out to.
    address[] public topUsers;

    //startTime of the contract. I hope this is called only once.
    uint startTime = now;
    uint k = 5;
    uint kthTopSimilarity = 0;
    address public trainingDataset; //Needs to be initialised somewhere.
    address public model; //Needs to be initialised somewhere.
    bool payoutComplete = false;

    //Should be called when the dataset is uploaded elsewhere.
    //Store the user as having submitted, and link them to the pointer to the dataset.
    //address _dataset can be changed to whatever pointer is appropriate.
    function submit(address _participant, address _dataset) payable external {
        users.push(_participant);
        datasetsMap[_participant] = _dataset;

        similarityMap[_participant] = similarityDatasets(trainingDataset, _dataset);
        evaluationMap[_participant] = evaluateDataset(model, _dataset);

        //If this similarity is high enough to be in the top k, replace the lowest value in the topUsers list.
        //Find the new kthTopSimilarity and set it.
        if (similarityMap[_participant] > kthTopSimilarity) {
            if (topUsers.length < k) {
                topUsers.push(_participant);
                kthTopSimilarity = 0;
            } else {
                for (uint i = 0; i<topUsers.length; i++) {
                    if (similarityMap[topUsers[i]] == kthTopSimilarity) {
                        topUsers[i] = _participant;
                    }
                }
                uint256 newKthTopSimilarity = similarityMap[_participant];
                for (uint j = 0; i<topUsers.length; j++) {
                    if (similarityMap[topUsers[j]] < newKthTopSimilarity) {
                        newKthTopSimilarity = similarityMap[topUsers[j]];
                    }
                }
                kthTopSimilarity = newKthTopSimilarity;
            }
        }
    }

    function similarityDatasets(address _trainingSet, address _dataset) internal returns(uint256 similarityVal){
        //TODO - link to some external function that measures similarity between datasets here.
        return 0;
    }

    function evaluateDataset(address _model, address _dataset) internal returns(uint256 evaluationVal) {
        //TODO - link to some external function that evaluates _dataset on _model.
        return 0;
    }

    function amountToPay(uint similarityVal) internal returns(uint amount){
        //TODO - decide on an amount to pay depending on the similarity and k.
        return 0;
    }

    function draw() external {
        require(now - startTime > 2 weeks);
        require(!payoutComplete);
        for (uint i = 0; i<topUsers.length; i++) {
            topUsers[i].transfer(amountToPay(similarityMap[topUsers[i]]));
        }
        payoutComplete = true;
    }
}
