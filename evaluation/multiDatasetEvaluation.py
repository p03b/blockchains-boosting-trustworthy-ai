from EvaluatedDataset import EvaluatedDataset
from EvaluatedModel import EvaluatedModel
from SimilarityEvaluation import SimilarityEvaluation

# Given an EvaluatedDataset, fill in the confusion matrix.
def FillDatasetConfusionMatrix(dataset):
    (TP, TN, FP, FN) = GetConfusionMatrix(dataset.predicted, dataset.actual)
    dataset.TP = TP
    dataset.TN = TN
    dataset.FP = FP
    dataset.FN = FN

def FillDatasetSimilarity(dataset, testingSet, trainingSet, attrWeighting):
    dataset.similarity = SimilarityEvaluation.GetSimilarity(trainingSet, testingSet, attrWeighting)

def GetConfusionMatrix(predicted, actual):
    TP = 0
    TN = 0
    FP = 0
    FN = 0

    for i in range(0,len(predicted)):
        #Should be positive
        if (actual[i] == 1):
            if(predicted[i]==1):
                TP+=1
            else:
                FN+=1
        else:
            if(predicted[i]==0):
                TN+=1
            else:
                FP+=1

    return (TP, TN, FP, FN)
