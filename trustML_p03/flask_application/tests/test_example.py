
#pip3 install pytest
# in terminal cd to tests and type pytest
import sys
sys.path.append('../')

# Now you can import your module
import compile_solidity_utils as utils

def func(x):
    return x + 1

def test_answer():
    assert func(3) == 5
