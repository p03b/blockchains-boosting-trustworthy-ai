import unittest
from project_sample import *


class TestMathFunc(unittest.TestCase):
    """Test mathfuc.py"""

    def test_add_single_blockchain(self):
        """Test if we can add block into blockchain"""

        self.assertEqual(1, add_a_block("pima.csv").index)
        self.assertEqual("pima.csv", add_a_block("pima.csv").data)

    def test_add_single_blockchain(self):
        """Test if we can add many blocks into blockchain"""

        # we need +1 here because we need to count the genesis as well.
        self.assertEqual(21, add_blocks(20).__len__())

    def test_contract(self):
        """Test if we the data in a single block can be tested in the contract"""

        # as for this one, we can see the result directly to get a better understanding
        test_data("pima.csv")
        # only for this test I'll just leave it to be printed out because there are going to test the quality of ML,
        # we should use as many test as we can, and it would be better if we save the output and compare it with
        # the answer file. as for this project, I believed that it is enough for user to understand how our project
        # works, the real contract shall be wrote in Solidity, so I would like to leave it here so I can spend more
        # time on the real one.


if __name__ == '__main__':
    unittest.main()
