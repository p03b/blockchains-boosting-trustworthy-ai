from django.urls import path

from . import views

app_name = 'upload_model'
urlpatterns = [
    path('delete_model/<int:model_id>', views.delete_model, name = 'delete_model'),
    path('home/', views.home, name='home'),
    path('model_detail/<int:model_id>/',views.model_detail, name='model_detail'),
    path('uploading/',views.uploading,name='uploading'),
    path('upload_data/',views.upload_data,name="upload_data"),
    path('test/', views.test, name='test'),
    path('reward//<str:receiver>/<int:id>/<int:value>', views.reward, name='reward'),
]
